// contentQuiz;

import { data } from "../data/questions.js";
// const data
import {
  checkAnswerRadioButton,
  checkFillToInput,
  renderQuestion,
  showResult,
} from "./questionController.js";

let currentQuestionIndex = 0;

let arrQuestion = data.map((question) => {
  return { ...question, isCorrect: false };
});

// render lần đầu

renderQuestion(arrQuestion[currentQuestionIndex]);
document.getElementById("currentStep").innerHTML = `<h2> ${
  currentQuestionIndex + 1
} / ${arrQuestion.length}</h2>`;
// khi user nhấn next
document.getElementById("nextQuestion").addEventListener("click", function () {
  // khi làm xong câu hỏi cuối cùng
  if (arrQuestion[currentQuestionIndex].questionType == 1) {
    arrQuestion[currentQuestionIndex].isCorrect = checkAnswerRadioButton();
  } else {
    arrQuestion[currentQuestionIndex].isCorrect = checkFillToInput();
  }
  currentQuestionIndex++;

  if (currentQuestionIndex >= arrQuestion.length) {
    showResult();
    return;
  }
  // show số thứ tự
  document.getElementById("currentStep").innerHTML = `<h2> ${
    currentQuestionIndex + 1
  } / ${arrQuestion.length}</h2>`;
  renderQuestion(arrQuestion[currentQuestionIndex]);
});
